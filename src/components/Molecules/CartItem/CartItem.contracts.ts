import type { ComponentPropsOptions } from '@vue/runtime-core'
import type { CartItemI } from '@/shared_contracts/cart/cart'

export interface CartItemProps extends CartItemI {
  isDisabled: boolean,
  hasActions: boolean
}

export const cartItemProps: ComponentPropsOptions<CartItemProps> = {
  id: {
    type: String,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  createdAt: {
    type: String,
    required: true
  },
  product: {
    type: Object,
    required: true
  },
  isDisabled: {
    type: Boolean,
    required: true
  },
  hasActions: {
    type: Boolean,
    required: false,
    default: () => true
  }
}

export default {}
