import type { ComponentPropsOptions } from '@vue/runtime-core'
import type { ProductI } from '@/shared_contracts/product/product'

export interface ProductCard extends ProductI {
  cbAddProduct: (productId: string) => Promise<void> | void,
  disabled: boolean
}

export const productProps: ComponentPropsOptions<ProductCard> = {
  sku: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  finalPrice: {
    type: Object,
    required: true
  },
  regularPrice: {
    type: Object,
    required: true
  },
  discountOff: {
    type: Object,
    required: true
  },
  image: {
    type: Object,
    required: true
  },
  cbAddProduct: {
    required: true
  },
  disabled: {
    type: Boolean,
    required: true
  }
}
