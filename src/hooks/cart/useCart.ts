import { useFetch } from '@/hooks/useFetch'
import { ADD_ITEM_TO_CART_URL, REMOVE_ITEM_FROM_CART_URL } from '@/hooks/cart/cart.config'
import { useCartStore } from '@/stores/cart/cart'
import type { AddCartItem, CartItemI, RemoveCartItem } from '@/shared_contracts/cart/cart'

export const useCart = () => {
  const storeCart = useCartStore()

  const addToCart = async (productItem: AddCartItem): Promise<CartItemI> => {
    storeCart.isLoadingCart = true
    const hookUseFetchAddProduct = useFetch<CartItemI>(
      `${import.meta.env.VITE_BASE_URL_BACKEND}/${ADD_ITEM_TO_CART_URL}/${storeCart.cart.id}`,
      {
        method: 'PUT',
        body: JSON.stringify(productItem)
      }
    )

    const productEntity = await hookUseFetchAddProduct.fetchData()
    const selectedProduct = storeCart.cart.items.find(item => item.id === productEntity.id)
    if (!selectedProduct) {
      storeCart.cart.items.push(productEntity)
    } else {
      selectedProduct.quantity = productEntity.quantity
    }


    storeCart.isLoadingCart = false
    return productEntity
  }

  const removeFromCart = async (productItem: RemoveCartItem) => {
    storeCart.isLoadingCart = true
    const hookUseFetchDeleteProduct = useFetch<CartItemI>(
      `${import.meta.env.VITE_BASE_URL_BACKEND}/${REMOVE_ITEM_FROM_CART_URL}/${storeCart.cart.id}`,
      {
        method: 'DELETE',
        body: JSON.stringify(productItem)
      }
    )
    try {
      await hookUseFetchDeleteProduct.fetchData()
      const indexOfDeletingItem = storeCart.cart.items.findIndex((cartItem) => cartItem.product.sku === productItem.productSku)
      if (indexOfDeletingItem === -1) {
        throw new Error(`Cart item with sku: ${productItem.productSku} doesn't exist in pinia.`)
      }
      storeCart.$state.cart.items.splice(indexOfDeletingItem, 1)
    } catch (e) {
      return e
    } finally {
      storeCart.isLoadingCart = false
    }
  }

  return { addToCart, removeFromCart }
}
