import { useAuthStore } from '@/stores/auth'
import { watch } from 'vue'
import { useCartStore } from '@/stores/cart/cart'
import { useFetch } from '@/hooks/useFetch'
import { CART_ID, CREATE_CART_URL, GET_CART_ITEMS_URL } from '@/hooks/cart/cart.config'
import type { CartI, CartItemI, CreateEmptyCartI } from '@/shared_contracts/cart/cart'
import { useStorage } from '@/hooks/useStorage/useStorage'
import type { CartStorageI } from '@/hooks/cart/cart.contracts'

export const createCart = () => {
  // store auth
  const auth = useAuthStore()
  watch(() => auth.isUserWaitingForLoggedIn, async () => {
    await resolveCart()
  })

  const hookUseStorageCart = useStorage<CartStorageI>(CART_ID)
  // store cart
  // hook use fetch
  const storeCart = useCartStore()
  const hookUseFetchCreateCart = useFetch<CreateEmptyCartI>(
    `${import.meta.env.VITE_BASE_URL_BACKEND}/${CREATE_CART_URL}`,
    { method: 'PUT' }
  )

  const resolveCart = async () => {
    let cartId = hookUseStorageCart.get()?.cartId ?? ''
    // todo check cartId if is still valid

    if (!cartId) {
      cartId = (await hookUseFetchCreateCart.fetchData()).cartId
      hookUseStorageCart.set({ cartId })
    }

    storeCart.cart.id = cartId

    try {
      const hookUseFetchGetCartItems = useFetch<CartI | null>(
        `${import.meta.env.VITE_BASE_URL_BACKEND}/${GET_CART_ITEMS_URL}/${storeCart.cart.id}`)
      const cartEntity = await hookUseFetchGetCartItems.fetchData()
      storeCart.cart.items = cartEntity ? cartEntity.items : []
      storeCart.isLoadingCart = false
    } catch (e) {
      setTimeout(() => {
        resetCart()
        resolveCart()
      }, 10000)
      console.log(e)
    }
  }

  const resetCart = () => {
    hookUseStorageCart.clear()
  }
}
