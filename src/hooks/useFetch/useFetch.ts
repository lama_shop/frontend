export function useFetch<T> (url: string, config: RequestInit = {}) {
  const fetchData = async (): Promise<T> => {
    try {
      const res = await fetch(url, {
        headers: {
          'Content-Type': 'application/json'
        }, ...config
      })
      const resJson = await res.json()
      return resJson
    } catch (e) {
      throw e
    }
  }
  return { fetchData }
}
