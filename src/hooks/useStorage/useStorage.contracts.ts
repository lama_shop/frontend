export interface UseStorageOptions {

}

export interface UseStorage<T> {
  get: () => T | null
  set: (state: T) => void
  clear: () => void
}
