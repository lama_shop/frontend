import type { UseStorage, UseStorageOptions } from '@/hooks/useStorage/useStorage.contracts'

export const useStorage = <T> (key: string, options: UseStorageOptions = {}): UseStorage<T> => {
  const get = (): T | null => {
    const localStorageProperty = window.localStorage.getItem(key)

    if (!localStorageProperty) {
      return null
    }
    return JSON.parse(localStorageProperty)
  }

  const set = (state: T) => {
    window.localStorage.setItem(key, JSON.stringify(state))
  }

  const clear = () => {
    window.localStorage.removeItem(key)
  }
  return { get, set, clear }
}
