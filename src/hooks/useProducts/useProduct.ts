import { useFetch } from '@/hooks/useFetch'
import { ENDPOINT_PRODUCT } from '@/hooks/useProducts/useFetch.contracts'

export const useProduct = <T> (basePath: string = import.meta.env.VITE_BASE_URL_BACKEND) => {
  const hookUseFetch = useFetch<T>(`${basePath}/${ENDPOINT_PRODUCT}`)
  const getProducts = async (): Promise<T> => {
    const products = await hookUseFetch.fetchData()

    return products
  }

  return { getProducts }
}
