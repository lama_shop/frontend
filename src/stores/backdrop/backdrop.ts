import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useBackdropStore = defineStore('backdrop', () => {
  const isBackdrop = ref(false)


  const activateBackdrop = () => {
    isBackdrop.value = true

    // todo remove
    setTimeout(() => {
      isBackdrop.value = false
    }, 250)
  }

  const deactivateBackdrop = () => {
    isBackdrop.value = false
  }

  return {
    isBackdrop,
    activateBackdrop,
    deactivateBackdrop
  }
})
