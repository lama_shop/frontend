import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useAuthStore = defineStore('auth', () => {
  const isUserLoggedIn = ref(false)
  const isUserWaitingForLoggedIn = ref(true)

  const fetchUser = async () => {
    setTimeout(() => {
      isUserLoggedIn.value = true
      isUserWaitingForLoggedIn.value = false
    }, 500)
  }

  return {
    isUserLoggedIn,
    isUserWaitingForLoggedIn,
    fetchUser
  }
})

// export const useAuthStore = defineStore('auth', {
//   state: () => ({
//     isUserLoggedIn: false,
//     isUserWaitingForLoggedIn: true
//   }),
//   actions: {
//     async fetchUser () {
//       console.log('abc')
//       setTimeout(() => {
//         this.isUserLoggedIn = true
//         this.isUserWaitingForLoggedIn = false
//         console.log('test')
//       }, 2000)
//     }
//   }
// })
