import { ref } from 'vue'
import { defineStore } from 'pinia'
import { DrawerRegister } from '@/components/Organisms/Drawer/Drawer.config'

export const useDrawerStore = defineStore('drawer', () => {
    const componentToRender = ref('')
    const isShowDrawer = ref(false)


    const openDrawer = (componentName: string) => {
      const isRegisteredDrawerComponent =
        !!Object.keys(DrawerRegister)
          .find(name => name === componentName)

      if (!isRegisteredDrawerComponent) {
        console.error(`[${componentName}] is not registered in drawer components`)
        return
      }
      componentToRender.value = componentName
      isShowDrawer.value = true
    }

    const closeDrawer = () => {
      console.log('closeDrawer')
      isShowDrawer.value = false
    }

    // watch(isShowDrawer, (value, oldValue, onCleanup) => {
    //   console.log('hmm')
    // })

    return {
      openDrawer, closeDrawer, componentToRender, isShowDrawer
    }
  }
)
