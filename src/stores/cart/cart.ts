import { defineStore } from 'pinia'
import { computed, reactive, ref, watch } from 'vue'
import type { CartI } from '@/shared_contracts/cart/cart'

export const useCartStore = defineStore('cart', () => {
  // todo cart
  const isLoadingCart = ref(true)

  const cart = reactive<Partial<CartI> & Pick<CartI, 'items'>>({
    id: undefined,
    items: []
  })

  const productCartQuantity = computed(() => cart.items.reduce((prevValue, currentValue) => {
    return prevValue + currentValue.quantity
  }, 0))

  const productCartsTotalGross = computed(() => +cart.items.reduce(((
    prevValue,
    cartItem
  ) => prevValue + cartItem.product.finalPrice.gross * cartItem.quantity), 0).toFixed(2))

  const productCartsTotalNet = computed(() => +cart.items.reduce(((
    prevValue,
    cartItem
  ) => prevValue + cartItem.product.finalPrice.net * cartItem.quantity), 0).toFixed(2))

  const tax = computed(() => (productCartsTotalGross.value - productCartsTotalNet.value).toFixed(2))

  return {
    isLoadingCart,
    cart,
    productCartQuantity,
    productCartsTotalGross,
    productCartsTotalNet,
    tax
  }
})
