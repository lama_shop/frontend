import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import './assets/main.scss'
import { DrawerRegister } from '@/components/Organisms/Drawer/Drawer.config'

const app = createApp(App)
app.config.globalProperties.$filters = {
  currency (value: number) {
    return value + ' zł'
  }
}
for (const [key, value] of Object.entries(DrawerRegister)) {
  //@ts-ignore
  app.component(key, value)
}

app.use(createPinia())
app.use(router)

app.mount('#app')
